<?php
/**
 * Wordpress Web Starter Kit functions and definitions
 *
 * @package Wordpress Web Starter Kit
 */


if ( ! function_exists( 'wwsk_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function wwsk_setup() {

  /*
   * Enable support for Post Thumbnails on posts and pages.
   *
   * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
   */
  
  add_theme_support( 'post-thumbnails' );

  // This theme uses wp_nav_menu() in one location.
  register_nav_menus( array(
    'primary' => __( 'Primary Menu', 'wwsk' ),
  ) );

}
endif; // wwsk_setup
add_action( 'after_setup_theme', 'wwsk_setup' );

/**
 * Register Roboto Condensed Google font
 */
function wwsk_font_url() {
  $font_url = '';
  /*
   * Translators: If there are characters in your language that are not supported
   * by RobotoCondensed, translate this to 'off'. Do not translate into your own language.
   */
  if ( 'off' !== _x( 'on', 'Roboto Condensed font: on or off', 'wwsk' ) ) {
    $font_url = add_query_arg( 'family', urlencode( 'Roboto Condensed:300,400,700,300italic,400italic,700italic' ), "//fonts.googleapis.com/css" );
  }

  return $font_url;
}


/**
 * Enqueue scripts and styles.
 */
function wwsk_scripts() {

  // Add RobotoCondensed font, used in the main stylesheet.
  // wp_enqueue_style( 'wwsk-robotocondensed', wwsk_font_url(), array(), null );

  // Enqueue main stylesheet
  wp_enqueue_style( 'wwsk-style', get_template_directory_uri() . '/styles/main.min.css' );

  // Enqueue js main file in the Footer area
  wp_enqueue_script( 'wwsk-site', get_template_directory_uri() . '/scripts/site.min.js', array(), false, true );


  if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
    wp_enqueue_script( 'comment-reply' );
  }
}

add_action( 'wp_enqueue_scripts', 'wwsk_scripts' );

/**
 * Removing some WP-related elements from the header
 */
function wwsk_cleanheader() {
  remove_action('wp_head', 'rsd_link');
  remove_action('wp_head', 'wlwmanifest_link');
  remove_action('wp_head', 'wp_generator');
}

add_action('init', 'wwsk_cleanheader');

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';
'use strict';

// Include Gulp & Tools We'll Use
var gulp = require('gulp');

// Include plug-ins
var del = require('del');
var plugin = require('gulp-load-plugins')();
var browserSync = require('browser-sync');
var reload = browserSync.reload;
var pagespeed = require('psi');
var runSequence = require('run-sequence');

var AUTOPREFIXER_BROWSERS = [
  'ie >= 10',
  'ie_mob >= 10',
  'ff >= 30',
  'chrome >= 34',
  'safari >= 7',
  'opera >= 23',
  'ios >= 7',
  'android >= 4.4',
  'bb >= 10'
];

// Lint JavaScript
gulp.task('jshint', function () {
  return gulp.src('assets/scripts/**/*.js')
    .pipe(reload({stream: true, once: true}))
    .pipe(plugin.jshint())
    .pipe(plugin.jshint.reporter('jshint-stylish'))
    .pipe(plugin.if(!browserSync.active, plugin.jshint.reporter('fail')));
});

// Compile and Automatically Prefix Stylesheets
gulp.task('styles', function () {
  // For best performance, don't add Sass partials to `gulp.src`
  return gulp.src([
    'assets/styles/*.scss',
    'assets/styles/**/*.css',
    'assets/styles/components/components.scss'
  ])
    .pipe(plugin.changed('styles', {extension: '.scss'}))
    .pipe(plugin.sass({
      precision: 10
    }))
    .on('error', console.error.bind(console))
    .pipe(plugin.autoprefixer({browsers: AUTOPREFIXER_BROWSERS}))
    .pipe(gulp.dest('.tmp/styles'))
    // Concatenate And Minify Styles
    .pipe(plugin.rename({suffix: '.min'}))
    .pipe(plugin.if('*.css', plugin.csso()))
    .pipe(gulp.dest('styles'))
    .pipe(plugin.size({title: 'styles'}));
});

// Concatenate and minify JS
gulp.task('scripts', function() {
    return gulp.src(['assets/scripts/vendor/*.js', 'assets/scripts/source/*.js'])
    .pipe(plugin.concat('site.js'))
    .pipe(plugin.rename({suffix: '.min'}))
    .pipe(plugin.uglify())
    .pipe(gulp.dest('scripts'))
    .pipe(plugin.size({title: 'scripts'}));
});

// Optimize Images
gulp.task('images', function () {
  return gulp.src('assets/images/**/*')
    .pipe(plugin.newer('images'))
    .pipe(plugin.imagemin({
      progressive: true,
      interlaced: true
    }))
    .pipe(gulp.dest('images'))
    .pipe(plugin.size({title: 'images'}));
});

// Copy Fonts from the Assets folder
gulp.task('fonts', function () {
  return gulp.src(['assets/fonts/**'])
    .pipe(gulp.dest('fonts'))
    .pipe(plugin.size({title: 'fonts'}));
});

// Clean .tmp Directory
gulp.task('clean', del('.tmp'));

// Build Production Files/Folders, the Default Task
gulp.task('default', ['clean'], function (cb) {
  runSequence('styles', ['scripts', 'images', 'fonts'], cb);
});

// Watch Files For Changes & Reload
gulp.task('serve', ['styles'], function () {

  browserSync({
    notify: false,
    // Customize the BrowserSync console logging prefix
    logPrefix: 'WWSK',
    // Run as an https by uncommenting 'https: true'
    // Note: this uses an unsigned certificate which on first access
    //       will present a certificate warning in the browser.
    // https: true,
    // server: ['.tmp', 'app'],
    // Proxy: If you're using a vagrant box, copy your url below
    proxy: 'john.dev'
  });
 
  gulp.watch(['*.php'], reload);
  gulp.watch(['assets/styles/**/*.{scss,css}'], ['styles', reload]);
  gulp.watch(['assets/scripts/**/*.js'], ['scripts', reload]);
  gulp.watch(['assets/scripts/**/*.js'], ['jshint']);
  gulp.watch(['assets/images/**/*'], ['images', reload]);
});

// Run PageSpeed Insights
// Update `url` below to the public URL for your site
gulp.task('pagespeed', pagespeed.bind(null, {
  // By default, we use the PageSpeed Insights
  // free (no API key) tier. You can use a Google
  // Developer API key if you have one. See
  // http://goo.gl/RkN0vE for info key: 'YOUR_API_KEY'
  url: 'https://example.com',
  strategy: 'mobile'
}));
<?php
/**
 * The template part for displaying a message that posts cannot be found.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Wordpress Web Starter Kit
 */
?>

<section>
  <header>
    <h1><?php _e( 'Nothing Found', 'wwsk' ); ?></h1>
  </header><!-- .page-header -->
  
  <p><?php _e( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'wwsk' ); ?></p>
      <?php get_search_form(); ?>

</section><!-- .no-results -->

<?php
/**
 * The template for displaying search results pages.
 *
 * @package Wordpress Web Starter Kit
 */

get_header(); ?>

    <main role="main">

    <?php if ( have_posts() ) : ?>

      <header>
        <h1><?php printf( __( 'Search Results for: %s', 'wwsk' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
      </header><!-- .page-header -->

      <?php /* Start the Loop */ ?>
      <?php while ( have_posts() ) : the_post(); ?>

        <?php
        /**
         * Run the loop for the search to output the results.
         * If you want to overload this in a child theme then include a file
         * called content-search.php and that will be used instead.
         */
        get_template_part( 'content', 'search' );
        ?>

      <?php endwhile; ?>

      <?php wwsk_paging_nav(); ?>

    <?php else : ?>

      <?php get_template_part( 'content', 'none' ); ?>

    <?php endif; ?>

    </main><!-- #main -->

<?php get_footer(); ?>

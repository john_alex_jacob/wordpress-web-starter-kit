<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @package Wordpress Web Starter Kit
 */


get_header(); ?>
    
    <h1>Page not found</h1>

<?php get_footer(); ?>
